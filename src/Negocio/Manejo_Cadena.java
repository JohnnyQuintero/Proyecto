/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 *
 * @author JHONY QUINTERO
 */
public class Manejo_Cadena {
    
    public Manejo_Cadena() {

    }
    /**
     * Dadas dos cadenas de caracteres s1 y s2, 
     * estando s2 vacía,  copiar la cadena s1 en s2 
     * sin utilizar ningún método de la clase Array
     * @param s
     * @param s2 
     */
    public void metodoA(String s,String s2) {
        char s1[] = s.toCharArray();
        char otro[] = new char[s1.length];
        for (int i = 0; i < s1.length; i++) {
            otro[i] = s1[i];
        }
        System.out.println(otro);
    }
    /**
     * Dadas dos cadenas de caracteres s1 y s2  añadir una al final de la otra. 
     * @param s
     * @param s2 
     */
    public void metodoB(String s,String s2) {
        char s1[] = s.toCharArray();
        char otro[] = s2.toCharArray();
        char nueva[] = new char[s1.length + otro.length];
        int pos = 0;
        for (int i = 0; i < s1.length; i++) {
            nueva[i] = s1[i];
            pos++;
        }
        for (int i = pos, j = 0; i < nueva.length; i++, j++) {
            nueva[i] = otro[j];
        }
        System.out.println(nueva);
    }
    /**
     * Leer una serie de palabras de teclado e insertarlos en una matriz 
     * de forma que este permanezca ordenado en todo momento.
     * No se podrán aceptar palabras iguales
     * @param s 
     */
    public void metodoC(String s) {
        char palabras[][];
        String arreglo[] = s.split(" ");//Separo por espacios las palabras
        String palabra[] = eliminarPalabra(arreglo);//Eliminar palabras repetidas
        palabras = new char[palabra.length][];//Creo el tamaño de las filas
        for (int i = 0; i < palabra.length; i++) {//Recorro las filas
            if (palabra[i] == null) {
                continue;//Verifico que si la posicion eliminada esta null, la ignore
            }
            char[] sub = palabra[i].toCharArray();//Empiezo hacer por caracteres
            palabras[i] = new char[sub.length];//Creo el tamaño de las columnas para los caracteres
            this.ordenarCaracteres(sub);//Ordenar por filas
            for (int j = 0; j < sub.length; j++) {//Recorro las columnas
                palabras[i][j] = sub[j]; //LLeno la matriz con los caracteres
            }
        }
        //Mostrar la matriz
        for (int i = 0; i < palabras.length; i++) {
            if (palabras[i] == null) {
                continue;
            }
            for (int j = 0; j < palabras[i].length; j++) {
                System.out.print(palabras[i][j] + " ");
            }
            System.out.println("");
        }
    }
    /**
     * Eliminar palabras repetidas
     * Metodo c
     * @param arr
     * @return 
     */
    public String[] eliminarPalabra(String arr[]) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if (i != j) {
                    if (arr[i].equals(arr[j])) {
                        String p = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = p;
                    }
                }
            }

        }
        arr[arr.length - 1] = null;
        return arr;
    }
    /**
     * Ordenar los caracteres
     * Metoto C
     * @param caracteres
     * @return 
     */
    public char[] ordenarCaracteres(char[] caracteres) {
        for (int i = 0; i < caracteres.length; i++) {
            char aux = caracteres[i];
            int j;
            for (j = i - 1; j >= 0 && caracteres[j] > aux; j--) {
                caracteres[j + 1] = caracteres[j];
            }
            caracteres[j + 1] = aux;
        }
        return caracteres;
    }
    /**
     * Método que permita contar la cantidad de frases de una oración 
     * (frase es una sucesión de palabras separadas por blancos y terminada en un punto) 
     * @param p 
     */
    public void metodoD(String p) {
        char caracter[] = p.toCharArray();
        int c = 0;
        for (int i = 0; i < caracter.length; i++) {
            boolean x = false;
            if (caracter[i] == '.') {
                x = true;
            }
            if (x) {
                c++;
            }
        }
        System.out.println(c);
    }
    /**
     * Método que permita tomar una frase e imprimirlas cada una líneas diferentes
     */
    public void metodoE() {
        //No lo entendi
    }
    /**
     * Método que determine si una cadena es palíndromo(devolverá true ó  false ). 
     * @param p
     * @return 
     */
    public boolean metodoF(String p) {
        char arreglo[] = p.toCharArray();
        int inicio = 0;
        int fin = arreglo.length - 1;
        boolean t = false;
        while ((inicio < fin) && (!t)) {
            if (arreglo[inicio] == arreglo[fin]) {
                inicio++;
                fin--;
            } else {
                t = true;
            }
        }
        return !t;
    }
    /**
     * Método  que tome dos cadenas L1 y L2 y  determine si L1 y L2 son similares. 
     * L1 y L2 son similares si tienen la misma longitud y la información de L1 esta en L2 en cualquier orden. 
     * @param l1
     * @param l2 
     */
    public void metodoG(String l1, String l2) {
        char[] c1 = l1.toCharArray();
        char[] c2 = l2.toCharArray();
        int c = 0;
        if (c1.length == c2.length) {
            for (int i = 0; i < c1.length; i++) {
                boolean x = false;
                for (int j = 0; j < c2.length; j++) {
                    if (c1[i] == c2[j]) {
                        x = true;
                    }
                }
                if (x) {
                    c++;
                }
            }
            if (c == c1.length || c == c2.length) {
                System.out.println("Las Cadenas son similares");
            } else {
                System.out.println("No son cadenas similares");
            }
        } else {
            System.out.println("No son cadenas similares");
        }
    }
    /**
     *Método que tome una cadena L1 y la convierta e Mayúsculas. (
     * Sin utilizar métodos de la clase String) 
     * @param l1 
     */
    public void metodoH(String l1) {
        char caracteres[] = l1.toCharArray();
        for (int i = 0; i < caracteres.length; i++) {
            caracteres[i] -= 32;
            System.out.print(caracteres[i] + " ");
        }
        System.out.println("");
    }
    /**
     * Método que tome una cadena L1 y la convierta e Minúsculas. 
     * (Sin utilizar métodos de la clase String)
     * @param l1 
     */
    public void metodoI(String l1) {
        char caracteres[] = l1.toCharArray();
        for (int i = 0; i < caracteres.length; i++) {
            caracteres[i] += 32;
            System.out.print(caracteres[i] + " ");
        }
        System.out.println("");
    }
    /**
     * Método que tome dos cadenas L1, L2, y  devuelva cuantas veces esta la cadena L2 en L1. 
     * Por ejemplo:   
     *          L1   Estaba Maria Mareada en el Mar 
     *          L2   Mar  
     *      Devolverá: ( 3 ).
     * @param l1
     * @param l2 
     */
    public void metodoJ(String l1, String l2) {
        char[] s1 = l1.toCharArray();
        char[] s2 = l2.toCharArray();
        int cont = 0, target = 0;
        for (int i = 0, j = 0; i < s1.length; i++) {
            if (s1[i] == ' ') {
                continue;
            }
            if (j < s2.length) {
                if (s2[j] == s1[i]) {
                    target++;
                }
                j++;
            } else {
                j = 0;
                i--;
            }
            if (target == s2.length) {
                cont++;
            }
        }
        System.out.println(cont);
    }
}
