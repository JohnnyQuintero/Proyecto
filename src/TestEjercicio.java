
import Negocio.Manejo_Cadena;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author JHONY QUINTERO
 */
public class TestEjercicio {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Manejo_Cadena cadena = new Manejo_Cadena();
        cadena.metodoA("Palabra","");
        cadena.metodoB("Eliza","palabra");
        cadena.metodoC("gato perro casa carro casa");
        System.out.println(cadena.metodoF("oso"));
        cadena.metodoG("Agua", "casa");
        cadena.metodoH("carro");
        cadena.metodoI("CARRO");
        cadena.metodoJ("Estaba Maria Mareada en el Mar", "Mar");
    }
}
